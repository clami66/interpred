#!/usr/bin/env python2.7
import os
import argparse
import itertools
from sys import argv
from sys import path
from glob import glob
from shutil import move, copy

from Bio.PDB import *
from Bio import SeqIO
import sys

#clean_started='/proj/wallner/users/x_lovso/exjobb/result_ip_seq/clean_up_started'
#if not os.path.exists(clean_started):
#    os.system('touch ' + clean_started)
#    os.system('/proj/wallner/apps/cleanup.sh')

#sys.exit()

#print sys.argv
#print "Killing generating too many files..."
#sys.exit()


###########################################################################################
#BASE = "/proj/wallner/apps/interpred"
#PDB = "/home/clami66/interpred/databases/PDB/"
BASE = "/proj/wallner/apps/interpred"
#Pdb = "/proj/wallner/users/x_clami/PDB/19052016/"
#PDB = "/proj/wallner/users/x_clami/PDB/07052018/"
#PDB = "/proj/wallner/share/PDB/191015_biounit/" #There is one /proj/wallner/share/PDB/191015_biounit_interfaces/ as well.
PDB = "/proj/wallner/share/PDB/20200324/" #There is one /proj/wallner/share/PDB/191015_biounit_interfaces/ as well. 

###########################################################################################

#Setup your HH suite and Modeller here if you want to do homology modelling################
#os.environ["HHDIR"] = "/home/clami66/hhsuite-2.0.15/"
#os.environ["HHLIB"] = "/home/clami66/hhsuite-2.0.15/"
#os.environ["HHUTIL"] = "/home/clami66/hhsuite-2.0.15/scripts/"
#os.environ["HHPDB"] = BASE + "/databases/pdb70"
#os.environ["HHUNI"] = BASE + "/databases/uniprot20_2016_02"
#os.environ["PYTHONPATH"] = "/usr/lib/modeller9.19/modlib/:" + str(os.getenv("PYTHONPATH"))

os.environ["HHDIR"] = "/proj/wallner/hh-suite/" #/proj/wallner/hhsuite-2.0.15/"
os.environ["HHUTIL"] = "/proj/wallner/hh-suite/scripts/"
#os.environ["HHPDB"] = "/proj/wallner/users/x_clami/PDB/pdb70_23Jun16/pdb70"
os.environ["HHPDB"] = "/proj/wallner/hh-suite-db/latest/pdb70"
os.environ["HHUNI"] = "/proj/wallner/hh-suite-db/UniRef30_2020_02/UniRef30_2020_02" #change to _02 once the database is finished
os.environ["PYTHONPATH"] = "/proj/wallner/modeller-9.13/modlib:" + str(os.getenv("PYTHONPATH")) + ":/proj/wallner/modeller/lib/x86_64-intel8/python2.5/"

###########################################################################################


TMP = BASE + "/tmp" #Temporary data folder
if not os.path.exists(TMP):
    os.mkdir(TMP)
DB = BASE + "/databases/"
OUT = BASE + "/output_files"
MODELS = BASE + "/homology_models"
SCRIPTS = BASE + "/scripts"
RFC = BASE + "/RandomForest"
TM = SCRIPTS + "/TMalignChain"

path.append(SCRIPTS)

os.environ['BASE'] = BASE
os.environ['TMPDIR'] = TMP
os.environ['MODELS'] = MODELS
os.environ['SCRIPTS'] = SCRIPTS
os.environ['DB'] = DB
os.environ['PDB'] = PDB
os.environ['RFC'] = RFC
os.environ['TM'] = TM
os.environ['OUT'] = OUT

import make_structural_alignments
import make_coarse_models
import make_homology_model

parser = argparse.ArgumentParser(description='Runs the InterPred pipeline.')
parser.add_argument('-fasta', nargs=2, metavar=('/path/to/target1.fasta', '/path/to/target2.fasta'),
                    help='Paths to locally stored fasta files of targets (needs HHblits setup)')
parser.add_argument('-pdb_local', nargs=4, metavar=('/path/to/target1.pdb', 'chain1', '/path/to/target2.pdb', 'chain2'),
                    help='Paths to locally stored pdb models of targets and their respective chains (if a pdb contains one chain with no name, use "_")')
parser.add_argument('-pdb_remote', nargs=4, metavar=('1abc', 'A', '2def', 'B'),
                    help='PDB ids of two targets and their respective chains (if a pdb contains one chain with no name, use \" \")')
parser.add_argument('-cpu', type=int, metavar=('#CPUs'),
                    help='Number of CPUs allotted (multithreading)')
parser.add_argument('-test', action='store_const', const='test', help='Run integration test')
parser.add_argument('-fast', action='store_const', const='fast', help='Run faster version seeded from sequence alignments')
parser.add_argument('-wdir', nargs=1, metavar=('/path/to/working/dir/'), help='Move the PDB to another location before running InterPred. Usually'
                                                                              'on a cluster node to avoid overloading I/O on the system.')
parser.add_argument('-skip_struct_ali', action='store_const', const='skip_struct_ali', help='Skips structural alignment and coarse modeling steps, i.e will only make hm models.')
parser.add_argument('-skip_coarse', action='store_const', const='skip_coarse', help='Skips coarse modeling step')

args = vars(parser.parse_args())
case = ""
cpus = 1
fast = False
skip_struct_ali=False
skip_coarse=False


class ChainSelect(Select):
    def __init__(self, ch):
        self.ch = ch
    def accept_chain(self, chain):
        return chain.get_id() in self.ch

def extract_chain_sequence(pdb, chain):
    parser = PDBParser()
    io = PDBIO()

    structure = parser.get_structure('pdb', pdb)

    for model in structure:
        if chain == "_":  # if no chain has been selected, we use the first chain
            chain = Selection.unfold_entities(structure, 'C')[0].get_id()

        if model.has_id(chain):

            io.set_structure(structure)
            io.save(pdb + '.chain', ChainSelect(chain))  # only save the selected chain
        else:
            return "error"

    structure = parser.get_structure(pdb + '.chain', pdb)
    ppb = CaPPBuilder()

    sequence_file = open(pdb + '.fasta', 'w')

    sequence_file.write('>' + pdb + '\n')

    for pp in ppb.build_peptides(structure):
        sequence_file.write(str(pp.get_sequence()))
    return pdb + '.fasta'

def run_fasta(fasta1, fasta2):
 
    OUT = os.path.dirname(os.path.abspath(fasta1))
    TMP = os.path.dirname(fasta1)
    MODELS = os.path.dirname(os.path.abspath(fasta1))

    os.environ['OUT'] = OUT

    try:
        os.environ['TMPDIR'] = os.environ['SNIC_TMP']
        if os.environ['TMPDIR']=='only-set-in-job-environment':
            os.environ['TMPDIR'] = OUT
    except KeyError:
        print('SNIC_TMP not set, setting TMPDIR to ' + OUT)
        os.environ['TMPDIR'] = OUT

    os.environ['MODELS'] = MODELS 
    bufsize = 0
    #print OUT
    #print TMP
    #"print MODELS
    log_file = open(OUT + "/log", 'w', bufsize)

    log_file.write("Making homology models for fasta: " + os.path.basename(fasta1) + "\n")

    print(fasta1)
    (hhr1, pdbs1) = make_homology_model.main(fasta1)
    print(fasta2)
    log_file.write("Making homology models for fasta: " + os.path.basename(fasta2) + "\n")
    (hhr2, pdbs2) = make_homology_model.main(fasta2)

    if not fast:
        hhr1 = ''
        hhr2 = ''

    pdb_chain1 = "_"
    pdb_chain2 = "_"

    if not skip_struct_ali:
        pdbs_aligns1 = []
        for pdb1 in pdbs1:
            log_file.write("Performing structural alignments for target " + os.path.basename(pdb1) + ". This might take up to a few hours.\n")
            alignment_file = make_structural_alignments.main((pdb1, pdb_chain1, hhr1))
            pdbs_aligns1.append((alignment_file, pdb1))

            pdbs_aligns2 = []
            for pdb2 in pdbs2:
                log_file.write("Performing structural alignments for target " + os.path.basename(pdb2) + ". This might take up to a few hours.\n")
                alignment_file = make_structural_alignments.main((pdb2, pdb_chain2, hhr2))
                pdbs_aligns2.append((alignment_file, pdb2))
        if not skip_coarse:
            log_file.write("Generating coarse models...\n")
#            print pdbs_aligns1
#            print pdbs_aligns2
#            import sys
#            sys.exit()
            
            for (alignment_file1_pdb1, alignment_file2_pdb2) in itertools.product(pdbs_aligns1, pdbs_aligns2):
#                print('hej')
                print (alignment_file1_pdb1[0], alignment_file1_pdb1[1], pdb_chain1, alignment_file2_pdb2[0], alignment_file2_pdb2[1], pdb_chain2)
#                continue
                make_coarse_models.main((alignment_file1_pdb1[0], alignment_file1_pdb1[1], pdb_chain1, alignment_file2_pdb2[0], alignment_file2_pdb2[1], pdb_chain2,log_file))

    
#    clean_up='cd ' + OUT +';zip sup_files.zip *sup*;rm *sup*'
#    clean_up='cd ' + OUT +';rm *sup*'
#    log_file.write(clean_up + '\n')
#    os.system(clean_up)
    log_file.write("Finished.")

def run_pdb_local(pdb1, pdb_chain1, pdb2, pdb_chain2):

    OUT = os.path.dirname(os.path.abspath(pdb1))
    TMP = OUT #os.path.dirname(pdb1)
    MODELS = OUT #os.path.dirname(pdb1)
#    print pdb1
#    print OUT
    os.environ['OUT'] = OUT
    os.environ['TMPDIR'] = OUT
    os.environ['MODELS'] = MODELS
    bufsize = 0
    log_file = open(OUT + "/log", 'w', bufsize)

    if not fast:
        hhr1 = ''
        hhr2 = ''

    if not fast:
        hhr1 = ''
        hhr2 = ''

    if fast:
        fasta1 = extract_chain_sequence(pdb1, pdb_chain1)
        fasta2 = extract_chain_sequence(pdb2, pdb_chain2)
        log_file.write("Making homology models for fasta: " + os.path.basename(fasta1) + "\n")
        (hhr1, ignore) = make_homology_model.main(fasta1)
        log_file.write("Making homology models for fasta: " + os.path.basename(fasta2) + "\n")
        (hhr2, ignore) = make_homology_model.main(fasta2)

    if not skip_struct_ali:
        log_file.write("Performing structural alignments for target " + os.path.basename(pdb1) + ". This might take up to a few hours.\n")
        alignment_file1 = make_structural_alignments.main((pdb1, pdb_chain1, hhr1))
        log_file.write("Performing structural alignments for target " + os.path.basename(pdb2) + ". This might take up to a few hours.\n")
        alignment_file2 = make_structural_alignments.main((pdb2, pdb_chain2, hhr2))
        if not skip_coarse:
            log_file.write("Generating coarse models...\n")
            make_coarse_models.main((alignment_file1, pdb1, pdb_chain1, alignment_file2, pdb2, pdb_chain2,log_file))
    log_file.write("Finished.")


def run_pdb_remote(pdb_id1, pdb_chain1, pdb_id2, pdb_chain2):

    bufsize = 0
    log_file = open(OUT + "/log", 'w', bufsize)

    pdb1 = TMP + '/' + pdb_id1 + '.pdb'
    pdb2 = TMP + '/' + pdb_id2 + '.pdb'

    pdbl = PDBList()
    pdbl.retrieve_pdb_file(pdb_id1, pdir=TMP)
    move(TMP + '/pdb' + pdb_id1 + '.ent', pdb1)
    pdbl.retrieve_pdb_file(pdb_id2, pdir=TMP)
    move(TMP + '/pdb' + pdb_id2 + '.ent', pdb2)

    if not fast:
        hhr1 = ''
        hhr2 = ''

    if fast:
        fasta1 = extract_chain_sequence(pdb1, pdb_chain1)
        fasta2 = extract_chain_sequence(pdb2, pdb_chain2)
        log_file.write("Making homology models for fasta: " + os.path.basename(fasta1) + "\n")
        (hhr1, ignore) = make_homology_model.main(fasta1)
        log_file.write("Making homology models for fasta: " + os.path.basename(fasta2) + "\n")
        (hhr2, ignore) = make_homology_model.main(fasta2)

    if not skip_struct_ali:
        log_file.write("Performing structural alignments for target " + os.path.basename(pdb1) + ". This might take up to a few hours.\n")
        alignment_file1 = make_structural_alignments.main((pdb1, pdb_chain1, hhr1))
        log_file.write("Performing structural alignments for target " + os.path.basename(pdb2) + ". This might take up to a few hours.\n")
        alignment_file2 = make_structural_alignments.main((pdb2, pdb_chain2, hhr2))
        if not skip_coarse:    
            log_file.write("Generating coarse models...\n")
            make_coarse_models.main((alignment_file1, pdb1, pdb_chain1, alignment_file2, pdb2, pdb_chain2))
    log_file.write("Finished.")

if args['cpu'] is not None:
    cpus = args['cpu']
    print("Using {} cpus".format(cpus))

os.environ['CPUS'] = str(cpus)


if args['fast'] is not None:
    fast = True
    print("Running faster version with sequence templates")

if args['skip_struct_ali'] is not None:
    skip_struct_ali=True
    print("Skipping structral alignment step")

if args['skip_coarse'] is not None:
    skip_coarse=True
    print("Skipping coarse modeling step")

if args['wdir'] is not None:

    SCRATCH = args['wdir'][0]
    import tarfile
    SCRATCH_PDB= SCRATCH + "/" + os.path.basename(PDB.strip("/"))

    #    print("Moving the PDB to dir " + SCRATCH)
    
    
    if not os.path.exists(SCRATCH_PDB):
#        ZIPPDB=os.path.dirname(PDB.strip("/")) + '/pdb.zip' #zip --symlinks -r pdb.zip 20200324/
#        unzip="unzip {} -d {}".format(ZIPPDB, SCRATCH)
#        if os.path.exists(ZIPPDB):
#            os.command(unzip)
        #        else:
        TARPDB = DB + '/pdb.tar'
        print("Extracting" + TARPDB + " to " + SCRATCH)
        if not (os.path.isfile(TARPDB)): #there is no .tar of the PDB
            print("Making a tarball of the PDB...")
            with tarfile.open(TARPDB, "w") as tar:
                tar.add(PDB, arcname=os.path.basename(PDB.strip("/")))

                #if not (os.path.isfile(SCRATCH + "/pdb.tar")):
                #    copy(TARPDB, SCRATCH)
                
                #    with tarfile.open(SCRATCH + "/pdb.tar", "r") as tar:
        with tarfile.open(TARPDB, "r") as tar:
            tar.extractall(path=SCRATCH)

    os.environ['PDB'] = SCRATCH_PDB
    print(os.environ['PDB'])
    #sys.exit()
if args['test'] is not None:

    import test_interpred

    test_interpred.main()

else:
    for arg in ['fasta', 'pdb_local', 'pdb_remote', 'test']:
        if args[arg] is not None:
            method = arg
            parameters = args[arg]

            if method == 'pdb_remote':
                run_pdb_remote(parameters[0], parameters[1], parameters[2], parameters[3])
            elif method == 'pdb_local':
                run_pdb_local(parameters[0], parameters[1], parameters[2], parameters[3])
            elif method == 'fasta':
                run_fasta(parameters[0], parameters[1])
            else:
                parser.print_help()

