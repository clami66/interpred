import os
import re
from sys import argv
from subprocess import call
from subprocess import check_output
from tempfile import mkstemp
from shutil import move
from os import remove
from glob import glob


from modeller import *  # Load standard Modeller classes, the PYTHONPATH should have been set by now
from modeller.automodel import *  # Load the automodel class


# generates hhblits alignments, formats them as a pir file, generates the python script for modeller and calls modeller upon it
def homology_this(fasta_file):
    try:
        BASE = os.environ["BASE"]
        MODELDIR = os.environ["MODELS"]
        HH = os.environ["HHDIR"]
        HHUTIL = os.environ["HHUTIL"]

        HHPDB = os.environ["HHPDB"]
        HHUNI = os.environ["HHUNI"]

        CPUS = os.environ["CPUS"]
        PDB = os.environ["PDB"]
        TMP = os.environ["TMPDIR"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    # fasta file of target protein
    target = fasta_file;

    id = os.path.splitext(os.path.basename(target))[0]

    if os.path.isfile(MODELDIR + "/" + id + "_0.pdb"):
        print("Model " + MODELDIR + "/" + id + "_0.pdb has already been generated" )
        homology_model_files = glob(MODELDIR + "/" + id + "_[0-9].pdb")
        return (TMP + "/" + id + ".sequence_templates", homology_model_files)


    # Alignments and template search
    hhr_out = MODELDIR + "/" + id + ".hhr"
    hhm_out = MODELDIR + "/" + id + ".hhm"
    if not (os.path.isfile(hhr_out)):
        # hhblits first alignments
        if not (os.path.isfile(hhm_out)):
            call([HH + "/bin/hhblits", "-i", target, "-d", HHUNI, "-oa3m", MODELDIR + "/" + id + ".a3m", "-cpu", CPUS, "-n", "2"])
            call([HH + "/bin/hhmake", "-i", MODELDIR + "/" + id + ".a3m"])

        # generating hhblits alignments with the pdb_70 db
        call([HH + "/bin/hhblits", "-i", MODELDIR + "/" + id + ".hhm", "-d", HHPDB, "-o", hhr_out]) #, "-Z", "10000"])
        print(HH + "/bin/hhblits", "-i", MODELDIR + "/" + id + ".hhm", "-d", HHPDB, "-o", hhr_out) #, "-Z", "10000")
    else:
        print("Protein id has already been aligned")
    # Template parsing
    hhr_template_file = open(hhr_out)
    templates = []
    sequence_templates = open(TMP + "/" + id + ".sequence_templates", 'w')
    sequence_templates_final = open(TMP + "/" + id + ".sequence_templates_final", 'w')
    for line in hhr_template_file:
        if re.match("[ 0-9]* [1-9]", line):
            split_line = line.split()
            template_number = split_line[0]
            template_upper   = split_line[1]
            template_name=template_upper[0:4].lower()+'_'+template_upper[5]
            evalue = float(line[41:48])

            cov_range = line[75:83].split("-")
            print(PDB + "/" + template_name[1:3] + "/" + template_name[0:4] + ".pdb")

            if os.path.isfile(PDB + "/" + template_name[1:3] + "/" + template_name[0:4] + ".pdb") or os.path.isfile(PDB + "/" + template_name[1:3] + "/pdb" + template_name[0:4] + ".ent"):
                templates.append((template_number, template_name, evalue, cov_range))
                sequence_templates.write(template_name[0:4] + " " + str(evalue)+ " " + template_name +"\n")
    sequence_templates.close()

    # Template filtering
    coverage_start_end = []
    final_templates = []

    coverage = {}
    total_coverage = 0

    add = True
    for template in templates:
        # check if the template is found in the PDB
        template_number = template[0]
        template_name = template[1]
        evalue = template[2]

        if evalue < 0.1:
            this_coverage_start = int(template[3][0])
            this_coverage_end = int(template[3][1])
            increase_coverage = 0

            for i in range(this_coverage_start, this_coverage_end+1):
                if i not in coverage:
                    increase_coverage += 1

            if float(increase_coverage)/(total_coverage+1) > 0.1: #increasing coverage of more than 10%?

                print("Adding template " + template_name + " increased coverage: " + str(increase_coverage) + "/" + str(total_coverage + 1))

                total_coverage += increase_coverage

                for i in range(this_coverage_start, this_coverage_end+1):
                    coverage[i] = True

                add = True

            else:
                add = False
                print("Rejecting template " + template_name + " increased coverage: " + str(increase_coverage) + "/" + str(total_coverage+1))
            #for last_coverage_start_end in coverage_start_end:
            #    #if this template is included completely in any previous template
            #    if this_coverage_start >= last_coverage_start_end[0] and this_coverage_end <= last_coverage_start_end[1]:
            #        add = False

            if add:
                coverage_start_end.append((this_coverage_start, this_coverage_end))
                final_templates.append((template_number, template_name))
                sequence_templates_final.write(template_name + " " + str(evalue) + " " + template_number + " " + str(this_coverage_start) + " " + str(this_coverage_end) + "\n")
                print("Added template:" + template_name + " " + str(evalue) + " " + str(this_coverage_start) + " " + str(this_coverage_end))
    sequence_templates_final.close()

    model_count = 0
    for template in final_templates:

        m = template[0]
        template_name = template[1]
        # generating pir alignments that will be fed to modeller
        call([HHUTIL + "hhmakemodel.pl", "-d", PDB + "/" + template_name[1:3], "-m", m, "-pir", MODELDIR + "/" + id + ".ali", "-i", MODELDIR +"/" + id + ".hhr"])
        print " ".join([HHUTIL + "hhmakemodel.pl", "-d", PDB + "/" + template_name[1:3], "-m", m, "-pir", MODELDIR + "/" + id + ".ali", "-i", MODELDIR +"/" + id + ".hhr"])

        ali_file = open(MODELDIR + "/" + id + ".ali")

        fh, abs_path = mkstemp()
        new_ali_file = open(abs_path, 'w')

        counter = 0
        for line in ali_file:
            if counter == 0: #re.match(">.*" + id, line):
                new_ali_file.write(">P1;" + id + "\n")
                counter += 1
            elif re.match(">.*", line):
                #change the id from 1abc_A to 1abc, the chain info should already be in the pir file
                new_ali_file.write(line[0:8] + "\n")
            elif re.match("structureX", line):
                #change the id from 1abc_A to 1abc, the chain info should already be in the pir file
                split_line = line.split(":")
                split_line[1] = split_line[1][0:4]
                new_ali_file.write(":".join(element for element in split_line))
            else:
                new_ali_file.write(line)
        new_ali_file.close()

        remove(MODELDIR + "/" + id + ".ali")
        move(abs_path, MODELDIR + "/" + id + ".ali")
        os.chmod(MODELDIR + "/" + id + ".ali", 0o664)

        CWD=os.getcwd()
        os.chdir(TMP)

        #Homology modeling by the automodel class
        log.verbose()  # request verbose output
        env = environ()  # create a new MODELLER environment to build this model in
        print(PDB + "/" + template_name[1:3])
        # directories for input atom files
        env.io.atom_files_directory = [PDB + "/" + template_name[1:3]]

        model = automodel(env, alnfile=MODELDIR + "/" + id + ".ali",  # alignment filename\n
              knowns=(template_name[0:4]), sequence = id)              # code of the target\n";
        model.starting_model= 1                 # index of the first model\n
        model.ending_model = 1  # index of the last model\n
        model.make()  # do the actual homology modeling";

        move(id + ".B99990001.pdb", MODELDIR + "/" + id + "_" + str(model_count) + ".pdb")
        model_count+=1
        os.chdir(CWD)
    return (TMP + "/" + id + ".sequence_templates", glob(MODELDIR + "/" + id + "_*" + ".pdb"))

def main(argv):
    return homology_this(argv)

if __name__ == '__main__':
    main(sys.argv[1:])
