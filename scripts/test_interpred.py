from __future__ import print_function

import filecmp
import os
from sys import path
from glob import glob
import pandas as pd

#It should be enough to set BASE as the installation directory and the PDB directory
BASE = "/proj/wallner/apps/interpred"
PDB = BASE + '/test/PDB_test/'  #Your PDB folder
####################################################################################

TMP = BASE + "/tmp/" #Temporary data folder
OUT = BASE + "/tmp/"
MODELS = BASE + "/homology_models"
SCRIPTS = BASE + "/scripts"
DB = BASE + '/test/'
RFC = BASE + "/RandomForest"
TM = SCRIPTS + "/TMalignChain"

path.append(SCRIPTS)

os.environ['BASE'] = BASE
os.environ['TMPDIR'] = TMP
os.environ['MODELS'] = MODELS
os.environ['SCRIPTS'] = SCRIPTS
os.environ['DB'] = DB
os.environ['PDB'] = PDB
os.environ['RFC'] = RFC
os.environ['TM'] = TM
os.environ['OUT'] = OUT

import make_structural_alignments
import make_coarse_models

def test():

    pdb1 = 'test/Q9UH73_0.pdb'
    pdb2 = 'test/P19838_0.pdb'

    os.environ['CONFIG'] = BASE + '/test/'

    print("\nTesting installation...\n")

    alignment_file1 = make_structural_alignments.main((pdb1, "_", ""))
    alignment_file2 = make_structural_alignments.main((pdb2, "_", ""))

    log_file = open("test/log", 'w')
    make_coarse_models.main((alignment_file1, pdb1, "_", alignment_file2, pdb2, "_", log_file))


    print("Testing structural alignment module... ", end=" ")

    for file in glob(TMP + "/*.structural_alignments"):
        base = os.path.basename(file)
        new_alignment = pd.read_csv(file)
        old_alignment = pd.read_csv('test/' + base)

        if not (new_alignment['tm1'].isin(old_alignment['tm1'].values).all(0) and new_alignment['tm2'].isin(old_alignment['tm2'].values).all(0)):#not filecmp.cmp(file, 'test/' + base):
            print("\n    Something is wrong with the structural alignment module. Check the installation.")
            exit(1)

    print("Done.\n")

    print("Testing coarse modelling module... ", end=" ")

    if not filecmp.cmp(TMP + "Q9UH73_0___P19838_0____features", 'test/Q9UH73_0___P19838_0____features'):
        print("\n    Something is wrong with the feature extraction (feature file: " + TMP + "Q9UH73_0___P19838_0____features does not look right). Check the installation.")
        exit(1)

    if not filecmp.cmp(TMP + "Q9UH73_0___P19838_0____predictions", 'test/Q9UH73_0___P19838_0____predictions'):
        print("\n    Something is wrong with the prediction function (Random Forest output: " + TMP + "Q9UH73_0___P19838_0____prediction does not look right). Check the installation.")
        exit(1)

    for file in glob(TMP + "/*.pdb"):
        base = os.path.basename(file)

        if not filecmp.cmp(file, 'test/' + base):
            print("    Something is wrong with the coarse modelling module (coarse model PDB: " + file + " does not look right). Check the installation.")
            exit(1)

    print("Done.\n")

    print("All good!")

def main():
    test()

if __name__ == '__main__':
    main(sys.argv[1:])
