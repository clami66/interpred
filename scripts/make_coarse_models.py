from __future__ import print_function

import os
from os.path import basename, splitext, dirname, abspath
from sys import argv
import re
import pandas as pd
import numpy as np
import glob as glob
import itertools
from functools import partial
from subprocess import call, check_output, CalledProcessError
from multiprocessing import Pool

from sklearn.ensemble import RandomForestClassifier
from sklearn.externals import joblib

import sys
FNULL = open(os.devnull, 'w')

def get_interface_features(sup1, sup2, template_name, template_chain1, template_chain2, target_aligned1, template_aligned1):

    try:
        SCRIPTS = os.environ["SCRIPTS"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    try:
        print(" ".join([SCRIPTS + "/" + "contact_map_chain", template_name, template_name, template_chain1, template_chain2]))

        cmapTemplate = check_output([SCRIPTS + "/" + "contact_map_chain", template_name, template_name, template_chain1, template_chain2])

    except CalledProcessError as ex:
        cmapTemplate = ex.output

    try:
        print(" ".join([SCRIPTS + "/" + "contact_map_chain", sup1, sup2, "A", "A"]))

        cmapModel   = check_output([SCRIPTS + "/" + "contact_map_chain", sup1, sup2, "A", "A"])
    except CalledProcessError as ex:
        cmapModel = ex.output

    if len(glob.glob("core*"))>0:
        print("Core file")
        sys.exit()
    cmapmodel_dictionary = {}
    cmaptemplate_dictionary = {}

    for line in cmapModel.split('\n'):
        line_split = line.split()
        if len(line_split) > 2:
            cmapmodel_dictionary[int(line_split[0])] = [int(element) for element in line_split[2:]]

    for line in cmapTemplate.split('\n'):
        line_split = line.split()
        if len(line_split) > 2:
            cmaptemplate_dictionary[int(line_split[0])] = [int(element) for element in line_split[2:]]

    target_align_counter = 0
    template_align_counter = 0

    model_contacts = 0
    template_contacts = 0
    shared_contacts = 0

    for i in range(0, len(target_aligned1)):

        if target_aligned1[i] != "-" and template_aligned1[i] != "-":
            target_align_counter += 1
            template_align_counter += 1

            if target_align_counter in cmapmodel_dictionary:
                model_contacts += len(cmapmodel_dictionary[target_align_counter])
            if template_align_counter in cmaptemplate_dictionary:
                template_contacts += len(cmaptemplate_dictionary[template_align_counter])
            if target_align_counter in cmapmodel_dictionary and template_align_counter in cmaptemplate_dictionary:
                shared_contacts += len([val for val in cmaptemplate_dictionary[template_align_counter] if val in cmapmodel_dictionary[target_align_counter]])
    print(shared_contacts, template_contacts, model_contacts)
    return (shared_contacts, template_contacts, model_contacts)

def make_rosetta_script(target1, target2, coarse_models_files):

    try:

        OUT = os.environ["OUT"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    rosetta_script_name = OUT + "/" + target1 + "_" + target2 + "_rosetta.sh"
    rosetta_script = open(rosetta_script_name, 'w')

    rosetta_script.write("#!/bin/bash\nROSETTABIN=\"/home/bjowa51/Rosetta/main/source/bin/\"\n"
                         "ROSETTARELAX=\"/home/bjowa51/Rosetta/main/source/bin/relax.default.linuxgccrelease\"\n"
                         "ROSETTADB=\"/home/bjowa51/Rosetta/main/database/\"\n"
                         "OUTPUTDIR=\"" + OUT + "\"\n"
                         "DBPARA=\"-database $ROSETTADB\"\n"
                         "PARTNERSPARA=\"-docking:partners A_B\"\n"
                         "OTHERPARA=\"-ignore_zero_occupancy\"\n"
                         "OUTPUTPARA=\"-out:file:silent $OUTPUTDIR/$base.silent  -out:file:scorefile $OUTPUTDIR/$base.sc\"\n"
                         "DOCKPARA=\"-dock_pert 5 12 -nstruct 1000\"\n\n")

#    for coarse_model_file in coarse_models_files:
#        base = os.path.splitext(os.path.basename(coarse_model_file))[0]
    rosetta_script.write("parallel $ROSETTARELAX -relax:constrain_relax_to_start_coords -nstruct 1 -out:pdb -out:path:pdb $OUTPUTDIR -database $ROSETTADB -in:file:s ::: " + OUT + "/target*_target*_pdb*_pdb*_[A-Z0-9a-z].pdb\n")
                            #"$ROSETTABIN/docking_protocol.default.linuxgccrelease $DBPARA -in:file:s $OUTPUTDIR/" + base + "_0001.pdb "
                             #"-out:file:silent $OUTPUTDIR/" + base + ".silent  -out:file:scorefile $OUTPUTDIR/" + base + ".sc $PARTNERSPARA $DOCKPARA $OUTPUTPARA $OTHERPARA\n")
    rosetta_script.close()

def make_models(target1, target2, predictions):

    print(target1, target2)
    try:

        TMP = os.environ["TMPDIR"]
        OUT = os.environ["OUT"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    coarse_model_names = []
    for prediction in predictions:

        template1 = prediction[0][0]
        template2 = prediction[0][1]

        target1_superposition = open(TMP + "/" + target1 + "_" + template1 + "_" + template2 + ".sup_all_atm")
        target2_superposition = open(TMP + "/" + target2 + "_" + template1 + "_" + template2 + ".sup_all_atm")

        coarse_model_name = OUT + "/" + target1 + "_" + target2 + "_" + template1 + "_" + template2 + ".pdb"
        target1_target2_superposition = open(coarse_model_name, 'w')

        target1_target2_superposition.write("TITLE     "+ target1 + " " + target2 + " in silico complex\n")
        target1_target2_superposition.write("EXPDTA    THEORETICAL MODEL GENERATED BY INTERPRED\n")
        target1_target2_superposition.write("REMARK   6 C. Mirabello, B.Wallner, 2017\n")
        target1_target2_superposition.write("REMARK   6 doi: 10.1002/prot.25280\n")
        target1_target2_superposition.write("REMARK   6 TEMPLATES: " + template1 + " " + template2 + "\n")
        target1_target2_superposition.write("REMARK   6 INTERPRED SCORE: " + str(prediction[1][1]) + "\n")

        pattern = re.compile('ATOM.*\sA\s.*\n')

        for line in target1_superposition:
            if re.match(pattern, line):
                target1_target2_superposition.write(line)

        target1_superposition.close()
        target1_target2_superposition.write("TER\n")

        for line in target2_superposition:
             if re.match(pattern, line):
                 chain_B_line = line[0:21] + 'B' + line[22:]
                 target1_target2_superposition.write(chain_B_line)

        target1_target2_superposition.close()
        coarse_model_names.append(coarse_model_name)

    return coarse_model_names


def getSeqID(pdb_file):

    # sequence Identity for the two models
    pattern = re.compile('.* SEQ ID:\s+(\d+\.\d+).*\n')

    pdb = open(pdb_file)
    pdbtext = pdb.read()

    pdb.close()

    try:
        seqID = float(pattern.findall(pdbtext)[0])
    except: #assume that this is a native PDB
        seqID = 100.00

    return seqID


def extract_features(shared_template, seqID1, seqID2):

    try:

        TMP = os.environ["TMPDIR"]
        TMC = os.environ["TM"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")
    #cols = ['pdb1', 'ch1_x', 'ch1_y', 'pdb2_x', 'pdb2_y', 'ch2_x', 'ch2_y', 'len2_x', 'len2_y', 'len1_x', 'len1_y',
    #        'ali_len_x', 'ali_len_y', 'rmsd_x', 'rmsd_y', 'tm1_x', 'tm1_y', 'tm2_x', 'tm2_y', 'ali1_x', 'ali2_x',
    #        'ali1_y', 'ali2_y']
    template_name = shared_template[0]
    temp_id = os.path.splitext(os.path.basename(template_name))[0]

    template_chain1 = shared_template[1]
    template_chain2 = shared_template[2]

    template_folder_pdb = template_name[1:3]

    target1 = shared_template[3]
    target2 = shared_template[4]
#    print(target1)
#    print(target2)

    id1 = os.path.splitext(os.path.basename(target1))[0]
    id2 = os.path.splitext(os.path.basename(target2))[0]

    target_chain1 = shared_template[5]
    target_chain2 = shared_template[6]

    if template_chain1 != template_chain2:

        length_target1 = shared_template[7]
        length_target2 = shared_template[8]
        length_template1 = shared_template[9]
        length_template2 = shared_template[10]
        alignment_length1 = shared_template[11]
        alignment_length2 = shared_template[12]

        rmsd1 = shared_template[13]
        rmsd2 = shared_template[14]

        tmscore1_1 = shared_template[15]
        tmscore1_2 = shared_template[16]
        tmscore2_1 = shared_template[17]
        tmscore2_2 = shared_template[18]

        sup1 = TMP + "/" + id1 + "_" + target_chain1 + "_" + temp_id + "_" + template_chain1 + "_" + temp_id + "_" + template_chain2 + ".sup"
        sup2 = TMP + "/" + id2 + "_" + target_chain2 + "_" + temp_id + "_" + template_chain1 + "_" + temp_id + "_" + template_chain2 + ".sup"
#        print(sup1)
#        print(sup2)
#        print('hej')
        # now launch TMalign between the two targets and the two $pdbA, $pdbB. Save the output pdb to calculate the interface maps
       # if not os.path.exists(sup1):
        try:
#            print(" ".join([TMC, '-B', target1, '-chB', target_chain1, '-A', template_name, '-chA', template_chain1, '-oa', sup1]))
            call([TMC, '-B', target1, '-chB', target_chain1, '-A', template_name, '-chA', template_chain1, '-oa', sup1], stdout=FNULL)
        except Exception as ex:
                print(ex.output)
                print("Error running:")
                print(" ".join([TMC, '-B', target1, '-chB', target_chain1, '-A', template_name, '-chA', template_chain1, '-oa', sup1]))
#                print(" ".join([TMC, '-B', target2, '-chB', target_chain2, '-A', template_name, '-chA', template_chain2, '-oa', sup2]))
        #else:
        #    print(sup1 + ' exists')

        #if not os.path.exists(sup2):
        try:
 #           print(" ".join([TMC, '-B', target2, '-chB', target_chain2, '-A', template_name, '-chA', template_chain2, '-oa', sup2]))
            call([TMC, '-B', target2, '-chB', target_chain2, '-A', template_name, '-chA', template_chain2, '-oa', sup2], stdout=FNULL)
        except Exception as ex:
            print(ex.output)
            print("Error running:")
            print(" ".join([TMC, '-B', target2, '-chB', target_chain2, '-A', template_name, '-chA', template_chain2, '-oa', sup2]))
            
#        else:
#            print(sup2 + ' exists')

        (common, interface_template, interface_model) = get_interface_features(sup1 + "_atm", sup2 + "_atm", template_name, template_chain1, template_chain2,
                                                                               shared_template[19], shared_template[20])

        return ("{}_{}".format(temp_id, template_chain1), "{}_{}".format(temp_id, template_chain2), common, interface_template, interface_model, seqID1, seqID2, length_target1, length_template1, length_target2, length_template2, alignment_length1, alignment_length2, rmsd1, rmsd2,tmscore1_1, tmscore1_2, tmscore2_1, tmscore2_2)


def main(args):

    try:
        RFC = os.environ["RFC"]

        CPUS = os.environ["CPUS"]
        TMP = os.environ["TMPDIR"]
        OUT = os.environ["OUT"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    MAX_INTERSECTION=1000000
    templates_target1 = args[0]
    pdb_file1 = args[1]
    target_chain1 = args[2]
    templates_target2 = args[3]
    pdb_file2 = args[4]
    target_chain2 = args[5]
    log_file=args[6]
    
    seqID1 = getSeqID(pdb_file1)
    seqID2 = getSeqID(pdb_file2)

    id1 = os.path.splitext(os.path.basename(templates_target1))[0]
    id2 = os.path.splitext(os.path.basename(templates_target2))[0]
    if os.path.exists(OUT + '/' + id1  + '_' + id2 + '_' + '_predictions'):
        print('Prediction file ' + OUT + '/' + id1  + '_' + id2 + '_' + '_predictions exists.\n')
        log_file.write('Prediction file ' + OUT + '/' + id1  + '_' + id2 + '_' + '_predictions exists.\n')
        return
    

    templates1 = pd.read_csv(templates_target1)
    templates2 = pd.read_csv(templates_target2)

    # Here will be saved the common templates
    intersection = pd.merge(templates1, templates2, how='inner', on=['pdb1'])

    intersection = intersection.loc[np.where(intersection['ch1_x'] != intersection['ch1_y'])]

    if intersection.size == 0:
        print("No structural templates could be found for couple:" + id1 + ", " + id2 + ". Now terminating.")
        log_file.write("No structural templates could be found for couple:" + id1 + ", " + id2 + ". Now terminating.\n")
        return
    if len(intersection) > MAX_INTERSECTION:
        print("Too many common templates " + str(len(intersection)) + " > " + str(MAX_INTERSECTION) + " for couple:" + id1 + ", " + id2 + ". Attempting stricter thresholds.\n")
        log_file.write("Too many common templates " + str(len(intersection)) + " > " + str(MAX_INTERSECTION) + " for couple:" + id1 + ", " + id2 + ". Attempting stricter thesholds.\n")

        intersection = intersection.loc[(((pd.to_numeric(intersection["tm1_x"]) >= 0.65) & (pd.to_numeric(intersection["tm2_x"]) >= 0.2))  |
                                         ((pd.to_numeric(intersection["tm1_x"]) >= 0.2) & (pd.to_numeric(intersection["tm2_x"]) >= 0.65))) &
                                        (((pd.to_numeric(intersection["tm1_y"]) >= 0.65) & (pd.to_numeric(intersection["tm2_y"]) >= 0.2))  |
                                         ((pd.to_numeric(intersection["tm1_y"]) >= 0.2) & (pd.to_numeric(intersection["tm2_y"]) >= 0.65)))]
        #return

    intersection.to_csv(OUT + '/' + id1 + '_' + id2 + '.common_templates')

    pool = Pool(processes=int(CPUS))


    cols = ['pdb1', 'ch1_x', 'ch1_y', 'pdb2_x', 'pdb2_y', 'ch2_x', 'ch2_y','len2_x', 'len2_y', 'len1_x', 'len1_y', 'ali_len_x', 'ali_len_y', 'rmsd_x', 'rmsd_y', 'tm1_x', 'tm1_y', 'tm2_x', 'tm2_y', 'ali1_x', 'ali2_x', 'ali1_y', 'ali2_y']
    intersection = intersection[cols]
   # print(intersection)
    features = pool.map(partial(extract_features, seqID1=seqID1, seqID2=seqID2), intersection.values)
    features = [x for x in features if x is not None]

    pool.close()
    pool.join()


    feature_outfile = open(TMP + '/' + id1 + '_' + id2 + '_' + '_features', 'w')
    print(TMP + '/' + id1 + '_' + id2 + '_' + '_features')
    predictions_outfile = open(OUT + '/' + id1  + '_' + id2 + '_' + '_predictions', 'w')
    print(OUT + '/' + id1  + '_' + id2 + '_' + '_predictions')

    for element in features:
        feature_outfile.write('\t'.join(str(column) for column in element) + "\n")

    rfc = joblib.load(RFC + '/randomforest.pkl')
 #   print(rfc)
    rfc.n_jobs=int(CPUS)
#    print(rfc)
#    print(CPUS)
    features_predictions = rfc.predict_proba(np.array(features)[:,2:])

    sorted_feature_predictions = sorted(zip(features, features_predictions), key=lambda x: x[1][1], reverse=True)

    coarse_models_files = make_models(id1, id2, sorted_feature_predictions[0:10])
    make_rosetta_script(id1, id2, coarse_models_files)

    for (feature, prediction) in sorted_feature_predictions:
        predictions_outfile.write(feature[0] + " " + feature[1] + " " + str(prediction[1]) + "\n")

if __name__ == '__main__':
    main(sys.argv[1:])
