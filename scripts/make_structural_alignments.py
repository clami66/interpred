from __future__ import print_function
import os
from os.path import basename, splitext, dirname, abspath
from sys import argv, exit
import re
import pandas as pd

from multiprocessing import Pool
from glob import glob
from functools import partial

from subprocess import call
from subprocess import check_output


import signal
from contextlib import contextmanager


class TooLongTimeException(Exception):
    def __init__(self, value):
        self.parameter = value
    def __str__(self):
        return repr(self.parameter)


def signal_handler(signum, frame):
    raise TooLongTimeException("Timed out!")


def run_tm_align_parse_output(template, target, target_chain="_"):
    # Runs TM align between the target and a template, parses the output and puts it in a dictionary
    
    try:

        TMC = os.environ["TM"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    signal.signal(signal.SIGALRM, signal_handler)
    signal.alarm(10)

    try:
        #print("Running " + " ".join([TMC, '-B', target, '-chB', target_chain, '-A', template[0], '-chA', template[1]]))
        tm_align_output = check_output([TMC, '-B', target, '-chB', target_chain, '-A', template[0], '-chA', template[1]])
    except TooLongTimeException as ex:
        #print("Command took too long to complete: " + TMC + ' -B ' + target + ' -chB ' + target_chain + ' -A ' + template[0] + ' -chA ' + template[1])
        return
    except Exception as ex:
        return
    finally:
        signal.alarm(0)


    #tm_data={}
    
    #for line in tm_align_output.split('\n'):
    #    match=re.search('(^.+)\schain:\s(\w).*',line)
    #    if match:
    #        print("MATCH: ")
    #    print(line)
    #    
    #import sys
    #sys.exit()
    pattern=re.compile((
#    '(.*\.[pe][dn][bt])\schain:\s(\w).*\n'
#    '(.*\.[pe][dn][bt])\schain:\s(\w).*\n' #\_(\w)
    '(^.+)\schain:\s(\w).*\n'
    '(^.+)\schain:\s(\w).*\n' #\_(\w)
    'Length of Chain_1:\s+(\d+).+\n'
    'Length of Chain_2:\s+(\d+).+\n\n'
    'Aligned length=\s*(\d+), RMSD=\s*([\d+\.]+), Seq_ID=n_identical\/n_aligned=\s*([\d+\.]+).*\n'
    'TM-score=\s*([\d+\.]+).*\n'
    'TM-score=\s*([\d+\.]+).*\n\n'
    '([A-Z-]+)\n'
    '^(.+)$\n'
    '([A-Z-]+)\n'
    ), re.MULTILINE)

    match = pattern.findall(tm_align_output)
    keys = ['pdb1', 'ch1', 'pdb2', 'ch2', 'len1', 'len2', 'ali_len', 'rmsd', 'seqid', 'tm1', 'tm2', 'ali1', 'dot_colon_str', 'ali2']
    #if match:
    #    print(match)
    for values in match:
      #  print(values)
        parsed_line = dict(zip(keys, values))
        return parsed_line
   # print(tm_align_output)

def get_PDB_file_list():
    PDB = os.environ["PDB"]
    #your list of templates
    list_of_templates = glob(PDB + '/*/[1-9][a-z0-9][a-z0-9][a-z0-9].pdb')
    print(PDB + '/*/[1-9][a-z0-9][a-z0-9][a-z0-9].pdb')

    if len(list_of_templates) == 0: #maybe the PDB contains .ent files?
        list_of_templates = glob(PDB + '/*/pdb[1-9][a-z0-9][a-z0-9][a-z0-9].ent')

    if len(list_of_templates) == 0:
        print("Error: couldn't file any .pdb or .ent file. Maybe the path tho the PDB is incorrect? "
              "Or maybe the PDB folder contains .ent.gz files?")
        exit()

    return list_of_templates

def check_for_template_list(sequence_alignments):#Checks for chains in the local PDB, builds a new list if none has been generated before

    try:

        DB = os.environ["DB"]
        PDB = os.environ["PDB"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    list_of_templates_chains = []
    path_to_templates_file = DB + "/" + "paths_to_templates"
    if not (os.path.isfile(path_to_templates_file)):

        print("    There is no cache of PDB files and chains in the system.\n    "
              "    Building the cache now (this operation is necessary only the first time a new PDB is detected)...")
        list_of_templates = get_PDB_file_list()

        paths_to_templates = open(path_to_templates_file, 'w')
        #cache the list of templates
        for pdb_file in list_of_templates:
            last_chain = "-"
            this_chain = "-"
            chains_in_pdb_file = []
            with open(pdb_file, 'r') as this_pdb_file:
                for line in this_pdb_file:
                    if re.match("ATOM", line):
                        this_chain = line[21]
                        if this_chain != last_chain:
                            last_chain = this_chain
                            chains_in_pdb_file.append(this_chain)
                #We shouldn't add single-chain PDBs
                if len(chains_in_pdb_file) > 1:
                    for chain in chains_in_pdb_file:
                        paths_to_templates.write(pdb_file + " " + chain + "\n")

        paths_to_templates.close()
        print("    Caching complete.")
        list_of_templates_chains = check_for_template_list(sequence_alignments)
        return list_of_templates_chains
    else:
        pdb_date = os.path.getmtime(PDB)
        paths_to_templates_date = os.path.getmtime(path_to_templates_file)
        if(paths_to_templates_date < pdb_date):
            print("    The internal file cache appears to be outdated. Is this a new PDB database in use?\n"
                  "    If so, delete file " + path_to_templates_file + " and restart InterPred")

        with open(path_to_templates_file) as paths_chains:
            for line in paths_chains:

                split_line = line.split()

                if len(sequence_alignments) > 0:
                    pdb_filename = os.path.splitext(os.path.basename(split_line[0]))[0]
                    if pdb_filename in sequence_alignments or pdb_filename[3:] in sequence_alignments:  #1abc or ent1abc
                        list_of_templates_chains.append(split_line)
                else:
                    list_of_templates_chains.append(split_line)
    return list_of_templates_chains

def get_hh_alignments(hhr_file):

    hhr = open(hhr_file)
    sequence_alignments = {}
    for line in hhr:
        #get hhblits template id
        sequence_alignments[line.split()[0]] = True
    hhr.close()

    return sequence_alignments

def main(argv):

    try:

        CPUS = os.environ["CPUS"]
        OUT = os.environ["OUT"]

    except KeyError:
        print("Environment variable not found. Please make sure that the InterPred.run script has been correctly setup")

    #if(len(argv) > 2):
    target_pdb = argv[0]
    target_chain = argv[1]
    hhr_file = argv[2]

    fast = False
    if hhr_file != '':
        fast = True

    id = os.path.splitext(os.path.basename(target_pdb))[0] + '_' + target_chain #1abc_A

    out_file = OUT + '/' + id + '.structural_alignments'
    print(target_pdb)
    print(out_file)
#    import sys
#    sys.exit()

    if os.path.isfile(out_file):
        print("PDB " + target_pdb + " has already been aligned.")
        return out_file
    hh_alignment_templates = {}
    if fast:
        hh_alignment_templates = get_hh_alignments(hhr_file)

    list_of_templates = check_for_template_list(hh_alignment_templates)

    pool = Pool(processes=int(CPUS))
    tm_align_hits = pool.map(partial(run_tm_align_parse_output, target=target_pdb, target_chain=target_chain), list_of_templates)
    pool.close()
    pool.join()

    tm_align_hits = [x for x in tm_align_hits if x is not None]
    df=pd.DataFrame(tm_align_hits)
    df.loc[(((pd.to_numeric(df["tm1"])>=0.5) & (pd.to_numeric(df["tm2"])>=0.1)) | ((pd.to_numeric(df["tm1"])>=0.1) & (pd.to_numeric(df["tm2"])>=0.5)))
                                                              & (pd.to_numeric(df["rmsd"])<4.0)].to_csv(out_file)

    return out_file

if __name__ == '__main__':
    main(sys.argv[1:])
