#!/bin/bash

#Specify the path to install directory
INSTALLDIR=$1

if [ -n $INSTALLDIR ]; then
	INSTALLDIR=$PWD
fi

echo "Compiling the custom version of TMalign"
cd TMalign_custom_code
make
cp TMalign ../scripts/TMalignChain

echo "Compiling the contact_map binary"
cd ../contact_map_code
make
cp contact_map_chain ../scripts/

cd ..

sed -i "s~BASE = .*~BASE = \"$INSTALLDIR\"~" InterPred.py
sed -i "s~BASE = .*~BASE = \"$INSTALLDIR\"~" scripts/test_interpred.py

echo "Please enter the full path to your local copy of PDB: "
read PDBPATH

sed -i "s~^PDB = .*~PDB = \"$PDBPATH\"~" InterPred.py

echo "Done. Run 'python2.7 InterPred.py -test' to check your installation"


