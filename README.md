# README #


# InterPred v0.1

InterPred is a software pipeline to predict and model 3D complexes of proteins.
(Mirabello, Claudio, and Björn Wallner. "InterPred: A pipeline to identify and model protein–protein interactions." 
Proteins: Structure, Function, and Bioinformatics 85.6 (2017): 1159-1170.)

## INSTALLATION

To setup InterPred you can clone the repository from your intended installation location
and run the install.sh script that will automatically determine the installation path and compile
some of the necessary binaries. The installation scripts will also prompt for the location of your PDB database
([See here](https://www.rcsb.org/pdb/static.do?p=download/ftp/index.html) if you don't have a copy of the
PDB on your machine)

If you move the interpred folder somewhere else, you will need to either rerun the install.sh
script or manually set the paths in the InterPred.py script (see "Configuration" paragraph).


## CONFIGURATION

InterPred can perform a homology modelling step if have no structure for your target proteins,
just the sequence (fasta files, one per target). In that case, you will need to download and configure:

* [HHblits](https://github.com/soedinglab/hh-suite);
* [Modeller](https://salilab.org/modeller/download_installation.html).

Once both are installed and ready to use, you will need to set the appropriate paths in the InterPred.py
script as follows:

```python
os.environ["HHDIR"] = /usr/local/hhsuite-X.Y.ZZ"                                          #hhsuite installation folder
os.environ["HHUTIL"] = "/usr/share/hhsuite/scripts/"                                      #path to other hhsuite scripts, 
                                                                                           usually under /usr/share/hhsuite/scripts
os.environ["HHPDB"] = BASE + "/databases/PDB/pdb70_08Mar14/pdb70_08Mar14"                 #path to the hhblits pdb70 database
os.environ["HHUNI"] = BASE + "/databases/uniprot/uniprot20_20XX_YY"                       #path to the hhblits uniprot20 database
os.environ["PYTHONPATH"] = "/usr/lib/modeller9.15/modlib:" + str(os.getenv("PYTHONPATH")) #path to the modeller libraries, 
                                                                                           usually under /usr/lib/modeller9.15
```

The other paths in the InterPred.py file can be left as is unless you want, for example, to store your
results or your temporary files somewhere else.


## DEPENDENCIES

InterPred runs on python2.7. We do not recommend using different versions of python, as it has not been
tested on those. If you have a different version of python on your system, we recommend using
[conda](https://conda.io/docs/user-guide/install/index.html) to create a virtual environment
under which you will be able to run InterPred without messing with your system.

InterPred will also need a few python libraries to work:

* numpy
* sklearn
* pandas
* biopython
* scipy

If these are not on your system, we recommend using pip to install them.

Installation example:

```sh
~: git clone git@bitbucket.org:clami66/interpred.git #clone InterPred repository
~: conda create -n py27 python=2.7                   #activate python2.7 virtual environment (if you want to use conda, skip otherwise)
~: source activate py27                              #activate conda environment
~: pip install biopython numpy sklearn pandas scipy  #install necessary python libraries
~: cd interpred/installation/directory               #go under the directory where the InterPred repo was cloned
~: ./install.sh                                      #run installation script
~: python2.7 InterPred.py -test                      #test InterPred (homology modelling step not tested)
```

## CONTACT

Claudio Mirabello (contact: clami66(at)ifm(dot)liu(dot)se )
Linköping University

## LICENSE

This program is free software; you can redistribute it and/or modify
it under the terms of the Version 2 (or later) GNU General Public License, as
published by the Free Software Foundation.

## CITATION

Mirabello, C., Wallner, B. (2017). InterPred: A pipeline to identify and model protein–protein interactions Proteins: Structure, Function, and Bioinformatics  85(6), 1159-1170.

