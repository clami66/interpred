#!/bin/bash

base=`basename $1`
dir=`dirname $1`

if [ $base == "target1.fasta" ]; then
	fast=$5
	/bin/python2.7 -u /home/clami66/interpred/InterPred.py $fast -fasta $1 $3 -cpu 4
elif [ $base == "target1.pdb" ]; then
	fast=$5
	/bin/python2.7 -u /home/clami66/interpred/InterPred.py $fast -pdb_local $1 $2 $3 $4 -cpu 4
fi

#Rosetta step
echo "" >> $dir/log
echo "Refining models" >> $dir/log

for file in $dir/target1*target2*rosetta.sh; do
	if [ -e "$file" ]; then
	chmod +x $file; $file
	else
		echo "Nothing to refine" >> $dir/log
		exit
	fi
done

for coarse in $dir/target1*target2*pdb*pdb*_[a-zA-Z0-9].pdb; do

	base=`basename $coarse .pdb`
	refined="$dir/${base}_0001.pdb"

	grep -v "^ATOM" $coarse | grep -v "^TER" > $dir/${base}_refined.pdb
	grep "^ATOM\|^TER" $refined  >> $dir/${base}_refined.pdb

done

echo "Refinement finished" >> $dir/log
