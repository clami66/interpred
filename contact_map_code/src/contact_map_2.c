#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include "molecule.h"

//
// Any comments and suggestion may be sent to:
// Author: Bj�rn Wallner
// E-mail: bjorn@sbc.su.se
//   
//


main(argc,argv)		/* Main routine */
     int argc;
     char *argv[];
{
  molecule      m[1], n[1];

  int           i,j,k,i2=0,j2=0;


  double        d;
  double        mean=0;
  int           atoms=0;
  int           residues=0;
  int           residues2=0;
  //  int           contacts[MAXRES][MAXRES]={{}};
  int **contacts;
  double **dist;
  
  //  int           res_contacts[20][20]={{}};
  //  int           restype[20]={};
  //  int           tot_res_contacts=0;
  /// int           type_i,type_j;
  int           current_res_i=0;
  int           current_res_j=0;
  long           selected_resnum;
  int           first=0;
  double        cutoff=0;
  FILE          *fp;
  int error=0;
  double temp;
  int tmp=0;
  int binary=1;
  int twoChains=1;
  int twoSeparateChains=0;
  char chain_name, chain_name2;

  char ch1[1], ch2[1];
  char chain2_name = '-';
  char chain1_name = '-';

  char prev_chain1_name = 'A';
  char prev_chain2_name = 'B';

  

  //float        sum=0;
  /* Parse command line for PDB filename */
  if(argc==2)
    {
      strcpy(m[0].filename,argv[1]);
      binary=0;
      twoChains=0;

    }
  else if(argc==3)
    {
      strcpy(m[0].filename,argv[1]);
//      cutoff=strtod(argv[2],(char**)(argv[2]+strlen(argv[2])));
      cutoff=strtod(argv[2],NULL);
      cutoff=cutoff*cutoff;
      twoChains=0;
    }  
  else if(argc==4)
    {
      strcpy(m[0].filename,argv[1]);
      cutoff=6.05;
      cutoff=cutoff*cutoff;
     
	chain1_name = argv[2][0];
	chain2_name = argv[3][0];
    }
  else if(argc==5)
    {
      strcpy(m[0].filename,argv[1]);
      strcpy(n[0].filename,argv[2]);
      cutoff=6.5;
      cutoff=36.5;//cutoff*cutoff;
     
	chain1_name = argv[3][0];
	chain2_name = argv[4][0];
	twoSeparateChains=1;
    }
  else
    {
      printf("Usage: contact_map [pdb_file] <cutoff(for binary otherwise distance)> <chain 1> <chain 2>\n");
      exit(1);
    }

  printf("%s %s\n",m[0].filename,n[0].filename);
  printf("%c %c\n",chain1_name,chain2_name);
  
  error=read_molecules(m,'a',chain1_name);



  if(error==0 && !twoSeparateChains)
    {  
      residues=m[0].residues;
      contacts = malloc(residues*sizeof(int *));
      dist = malloc(residues*sizeof(double *));
      for(i=0;i<residues;i++) {
	contacts[i] = malloc(residues*sizeof(int));
	dist[i] = malloc(residues*sizeof(double));
      }
      for(i=0;i<m[0].atoms;i++)  
	{
	  if(m[0].atm[i].rescount!=current_res_i)
	    {
	      for(j=i,current_res_j=current_res_i;j<m[0].atoms;j++)
		{

		  if(m[0].atm[j].rescount!=current_res_j)
		    {

		      d=crd(m,i,j);
		      if(contacts[current_res_i][current_res_j]==0 && d<cutoff)
			{

			  contacts[current_res_i][current_res_j]=1;
			  contacts[current_res_j][current_res_i]=1;

			}
		      dist[current_res_i][current_res_j]=d;
		      dist[current_res_j][current_res_i]=d;
		      current_res_j++;
		    }
		}				     
	      current_res_i++;
	    }
	}
      

      if(binary) {
	//printf("CUTOFF %lf\n",sqrt(cutoff));
      } else {
	printf("CUTOFF running in distance mode\n");
      }
      //printf("FORMAT: RES <counter> <resname> <residue> <chain>: <dist11> <dist12> ...\n");

     for(i=0;i<m[0].residues;i++)
	{

	    

	      chain_name=m[0].atm[m[0].CA_ref[i]].chain[0];
	      if(chain_name == ' ') {
		chain_name='_';
	      }
		//printf("%c %c\n",chain_name, chain1_name);

	      if(!twoChains){//simple case, doing all of the chains	      
	      	printf("RES %d %s %c %c: ",i+1,m[0].atm[m[0].CA_ref[i]].resname,m[0].sequence[i],chain_name);
		
	   	 for(j=0;j<m[0].residues;j++){
		  
		  if(binary) 
		    {
		      if(contacts[i][j]==1)
			{

			  chain_name=m[0].atm[m[0].CA_ref[j]].chain[0];
			  if(chain_name == ' ') {
			    chain_name='_';
			  }

				printf("%d %c%c ", m[0].atm[m[0].CA_ref[j]].resnum , m[0].sequence[j], chain_name);
  			  	
			}
		    } else {
		    printf("%lf ",sqrt(dist[i][j]));
		  }
		}
	      printf("\n");

	      }else{

		if(chain_name == chain1_name){
			i2++;
			//printf("%d %c%c: ", i2, m[0].sequence[i],chain1_name);
			printf("%d : ",i2);
			for(j=0;j<m[0].residues;j++){
		  
			  if(binary){

				chain_name=m[0].atm[m[0].CA_ref[j]].chain[0];

				if(chain_name == ' ') {
					chain_name='_';
				}

				if(chain_name == chain2_name){
					j2++;
				}
				if(contacts[i][j]==1){



					if(chain_name == chain2_name){
						
						//printf("%d %c%c ",j2, m[0].sequence[j],chain2_name);
						printf("%d ",j2);
	 				}


			  	}
			   

		   	 }else{
			    printf("%lf ",sqrt(dist[i][j]));
			}
		}
	 	j2=0;
		printf("\n");

		}
	      }


	    

	}
   

    }else if(error==0){
	//TWO CHAINS FROM TWO SEPARATE FILES CASE
	 
        error= read_molecules(n,'a',chain2_name);
	//printf("The error reading the file: %d", error);
	residues=m[0].residues;
	residues2=n[0].residues;
      contacts = malloc(residues*sizeof(int *));
      dist = malloc(residues*sizeof(double *));
      for(i=0;i<residues;i++) {
	contacts[i] = malloc(residues2*sizeof(int));
	dist[i] = malloc(residues2*sizeof(double));
      }
//printf("%d %d\n", m[0].atoms,n[0].atoms);
      for(i=0;i<m[0].atoms;i++)  
	{

	  if(m[0].atm[i].rescount!=current_res_i)
	    {
//printf("%d\t", current_res_i);
	      for(j=0,current_res_j=0;j<n[0].atoms;j++)
		{

		  if(n[0].atm[j].rescount!=current_res_j)
		    {

		      d=crd2(m,n,i,j);
//printf("d[%d][%d] = %f",  current_res_i, current_res_j,d);
		      if(contacts[current_res_i][current_res_j]==0 && d<cutoff)
			{

			  contacts[current_res_i][current_res_j]=1;
			  //contacts[current_res_j][current_res_i]=1;

			}
		      dist[current_res_i][current_res_j]=d;
		      //dist[current_res_j][current_res_i]=d;
		      current_res_j++;
		    }
		}	
//printf("\n");			     
	      current_res_i++;
	    }
	}
      



     for(i=0;i<m[0].residues;i++)
	{

	    

	      chain_name=m[0].atm[m[0].CA_ref[i]].chain[0];
	      if(chain_name == ' ') {
		chain_name='_';
	      }
		//printf("%c %c\n",chain_name, chain1_name);
		if(chain_name == chain1_name){
			i2++;
			//printf("%d %c%c: ", i2, m[0].sequence[i],chain1_name);
			printf("%d : ",i2);
			for(j=0;j<n[0].residues;j++){
		  


				chain_name2=n[0].atm[n[0].CA_ref[j]].chain[0];

				if(chain_name2 == ' ') {
					chain_name2='_';
				}

				if(chain_name2 == chain2_name){
					j2++;
				}
				if(contacts[i][j]==1){



					if(chain_name2 == chain2_name){
						
						//printf("%d %c%c ",j2, m[0].sequence[j],chain2_name);
						printf("%d ",j2);
	 				}


			  	}
			   


		}
	 	j2=0;
		printf("\n");

		}
	      


	    

	}
	}
}

